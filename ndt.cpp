#include <iostream>
#include <string>
#include <fstream>
#include <set>
#include <map>
#include <cmath>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl/registration/ndt.h>
#include <pcl/filters/approximate_voxel_grid.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

const int SIZEX = 2048; // max size of voxel array in x dimension
const int SIZEY = 2048; // max size of voxel array in y dimension
const int SIZEZ = 512; // max size of voxel array in z dimension
const int lSIZEX = 11; // log_2 of the above quantities;
const int lSIZEY = 11;
const int lSIZEZ = 9;
const double PI = 3.1415926535897932384626433832795028;

struct Gaussian {
    Eigen::Matrix3f Sigma;
    Eigen::Vector4f Mu;
    double zness;
};

struct MyVoxel {
    Gaussian g;
};

struct NNDT {
    // my own sparse voxel implementation with a lookup table
    // containing all the non-empty voxels and, for each voxel,
    // a lookup table containing all its non-empt neighbours
    std::map<int, MyVoxel> members;
    std::map<int, std::vector<MyVoxel*> > neighbours;
    float h;
};

inline int sub2ind(int ix, int iy, int iz) {
    return iz + (iy + ix * SIZEY) * SIZEZ;
}

NNDT fluffify(pcl::PointCloud<pcl::PointXYZ> &cloud, float h) {
    // converts point cloud into a fluffy mixture of Gaussians.
    std::map<int, std::set<int> > vox;
    for(int i=0, _i=cloud.points.size(); i<_i; i++) {
        // first we put the points into "pigeonholes", a sparse map
        // each point is put into both its own pigeonhole and the
        // neighbouring pigeonholes
        for(int dx=-1; dx<=1; dx++) {
            for(int dy=-1; dy<=1; dy++) {
                for(int dz=-1; dz<=1; dz++) {
                    int ind = sub2ind(
                        round(cloud.points[i].x/h) + dx + SIZEX/2, 
                        round(cloud.points[i].y/h) + dy + SIZEY/2,
                        round(cloud.points[i].z/h) + dz + SIZEZ/2
                    );
                    vox[ind].insert(i);
                }
            }
        }
    }
    NNDT nndt;
    nndt.h = h;
    for(std::map<int, std::set<int> >::iterator it = vox.begin();
            it != vox.end(); ++it) {
        // we iterate over all pigeonholes and compute their centroid
        // and covariance matrices, and keep it if it is a planar feature
        if(it->second.size() < 10) continue;
        int index = it->first;
        std::vector<int> indices(it->second.begin(), it->second.end());
        Gaussian g;
        pcl::compute3DCentroid(cloud, indices, g.Mu);
        pcl::computeCovarianceMatrix(cloud, indices, g.Mu, g.Sigma);
        //std::cerr << "points in the voxel" << it->second.size() << std::endl;
        //std::cerr << g.Mu << "\n" << g.Sigma << std::endl;
        Eigen::EigenSolver<Eigen::Matrix3f> eig(g.Sigma);
        Eigen::Vector3f eigenvalues = eig.eigenvalues().real();
        float mineig = 1e9; int mini = 0;
        float maxeig = -1e9; int maxi = 0;
        for(int i=0; i<3; i++) {
            if(eigenvalues[i] < mineig) {
                mineig = eigenvalues[i];
                mini = i;
            }
            if(eigenvalues[i] > maxeig) {
                maxeig = eigenvalues[i];
                maxi = i;
            }
        }
        int midi = 3 - mini - maxi;
        if(eigenvalues[midi] > 0.1 * eigenvalues[maxi]) {
            // only keep it if it is a planar-ish feature instead of a
            // mostly 1D row of points. The reason is that the latter
            // is likely to be because the points are from a single scan
            // line, which might not give good performance.
            Eigen::Vector3f z;
            z << 0, 0, 1;
            g.zness = std::abs(eig.eigenvectors().real().col(mini).dot(z));
            nndt.members[index].g = g;
        } else {
            //std::cerr << "Rejected: " << eigenvalues << std::endl;
        }
    }
    for(std::map<int, std::set<int> >::iterator it = vox.begin();
            it != vox.end(); ++it) {
        // we iterate over all pigeonholes to find their non-empty neighbours
        // for faster lookup in the future.
        int index = it->first;
        if(!nndt.members.count(index)) continue;
        int z = index & (SIZEZ-1);
        int y = (index >> lSIZEZ) & (SIZEY-1);
        int x = (index >> (lSIZEZ + lSIZEY)) & (SIZEX-1);
        for(int dx=-2; dx<=2; dx++) {
            for(int dy=-2; dy<=2; dy++) {
                for(int dz=-2; dz<=2; dz++) {
                    nndt.neighbours[sub2ind(x+dx,y+dy,z+dz)].push_back(
                            &(nndt.members[index]));
                }
            }
        }
    }
    return nndt;
}

double cost(NNDT &nndtx, NNDT &nndty, Eigen::Matrix3f rotation,
        Eigen::Vector3f translation) {
    // Evaluate the cost function of alignment between nndtx and nndty.
    // actually this is the negative cost function since the higher the
    // value, the better
    long double c = 0; // this is the cost
    for(std::map<int, MyVoxel >::iterator it = nndtx.members.begin();
            it != nndtx.members.end(); ++it) {
        int index = it->first;
        // get the index of the center of the current point cloud
        int z = index & (SIZEZ-1);
        int y = (index >> lSIZEZ) & (SIZEY-1);
        int x = (index >> (lSIZEZ + lSIZEY)) & (SIZEX-1);
        Eigen::Vector3f voxcenter;
        voxcenter << (x - SIZEX/2)*nndtx.h, (y - SIZEY/2)*nndtx.h, (z - SIZEZ/2)*nndtx.h;
        voxcenter = rotation * voxcenter + translation;
        // get the index in the other point cloud
        int indey = sub2ind(round(voxcenter[0]/nndty.h) + SIZEX/2,
                round(voxcenter[1]/nndty.h) + SIZEY/2,
                round(voxcenter[2]/nndty.h) + SIZEZ/2);
        if(!nndty.neighbours.count(indey)) continue;
        Gaussian g1 = it->second.g;
        Eigen::Vector3f Mu1 = g1.Mu.head(3);
        Mu1 = rotation * Mu1 + translation;
        Eigen::Matrix3f Sigma = rotation * g1.Sigma;
        long double newc2 = 0;
        for(int i=0, _i = nndty.neighbours[indey].size(); i<_i; i++) {
            Gaussian *g2 = &(nndty.neighbours[indey][i]->g);
            Eigen::Vector3f Mu2 = g2->Mu.head(3);
            long double newc = 1.0/std::sqrt((Sigma + g2->Sigma).determinant() * 2 * PI)
                * std::exp(-0.5 * (Mu1 - Mu2).dot( 
                    (Sigma + g2->Sigma).inverse() * (Mu1 - Mu2)));
            // penalize the Gaussians with surface normal close to the z axis
            newc *= (1.2 - g1.zness) * (1.2 - g2->zness);
            newc2 += newc;
            //std::cerr << "mu:" << std::endl << Mu1 << std::endl << Mu2 << std::endl;
        }
        c += newc2;
        //std::cerr << index << " " << indey << " " << newc2 << std::endl;
    }
    return c;
}

void visualize(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_X,
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_Y) {
    // Render two point clouds in my favourite colours ^__^
    boost::shared_ptr<pcl::visualization::PCLVisualizer>
        viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0.07, 0.07, 0.07);

    // draw cloud X
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>
        color_X (cloud_X, 255, 51, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud_X, color_X, "cloud X");
    viewer->setPointCloudRenderingProperties(
            pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud X");
    // draw cloud Y
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>
        color_Y (cloud_Y, 85, 136, 250); 
    viewer->addPointCloud<pcl::PointXYZ> (cloud_Y, color_Y, "cloud Y");
    viewer->setPointCloudRenderingProperties(
            pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud Y");
    // start visualizer
    viewer->addCoordinateSystem(1.0, "global");
    viewer->initCameraParameters();

    // wait until visualizer closed
    while(!viewer->wasStopped()) {
        viewer->spinOnce(100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }
}

int main (int argc, char** argv) {
    std::ofstream fout;
    fout.open("transformations7.dat");
    std::string filename;
    std::cin >> filename;
    // Loading first scan 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_Y (new pcl::PointCloud<pcl::PointXYZ>);
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud_Y) == -1) {
        PCL_ERROR (("Couldn't read file " + filename + "\n").c_str());
        return (-1);
    }
    std::cerr << "Loaded " << cloud_Y->size () 
        << " points from " << filename << std::endl;

    NNDT fluffy_Y = fluffify(*cloud_Y, 2.5);
    std::cerr << "Fluffy representation has: " << fluffy_Y.members.size() << endl;
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr fluffyCentroids_Y 
        (new pcl::PointCloud<pcl::PointXYZ>);

    for(std::map<int, MyVoxel >::iterator it = fluffy_Y.members.begin();
            it != fluffy_Y.members.end(); ++it) {
        fluffyCentroids_Y->push_back(pcl::PointXYZ(
                it->second.g.Mu[0], it->second.g.Mu[1], it->second.g.Mu[2]));
    }
    // visualize(cloud_Y, fluffyCentroids_Y);

    Eigen::Matrix4f T(Eigen::Matrix4f::Identity());

    fout << T << std::endl;
    // continually read the filename of the next point cloud from stdin
    while(std::cin >> filename) {
        // Set the X cloud as the cloud from the previous iteration
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_X = cloud_Y;
        NNDT fluffy_X = fluffy_Y;
        // Loading next scan 
        cloud_Y = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
        if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud_Y) == -1) {
            PCL_ERROR (("Couldn't read file " + filename + "\n").c_str());
            return (-1);
        }
        std::cerr << "Loaded " << cloud_Y->size () 
            << " points from " << filename << std::endl;

        fluffy_Y = fluffify(*cloud_Y, 2.5);
        std::cerr << "Fluffy representation has: " << fluffy_Y.members.size() << endl;
        // register X to Y
        for(int xxx=-127; xxx<=128; xxx+=2) {
            for(int yyy=-127; yyy<=128; yyy+=2) {
                Eigen::Matrix3f rotation;
                rotation << 1, 0, 0, 0, 1, 0, 0, 0, 1;
                Eigen::Vector3f translation;
                translation << xxx*0.02, yyy*0.02, 0;
                std::cout << " " << cost(fluffy_X, fluffy_Y, rotation, translation);
                std::cerr << ".";
            }
            std::cerr << std::endl;
            std::cout << std::endl;
        }
        visualize(cloud_X, cloud_Y);

        // registration complete, output result
        fout << T << std::endl;

        // get rid of useless things
        fluffy_X.members.clear();
        fluffy_X = fluffy_Y;
        cloud_X.reset();
    }
    return (0);
}
